
package Simulator;
/**
Register class
 */
public class Register 
{
    private int[][] GRR;
    private int[][] IXR;
    private int[] PC;
    private int PCvalue;
    private String[] PARA;
    private int[] FR;
    
    Register()
    {
        GRR = new int[4][16];
        IXR = new int[3][12];
        PC = new int[12];
        PARA = new String[7];
        FR = new int[2];
        
        for(int i = 0; i<GRR.length; i++)
        {
           for(int j = 0; j<GRR[0].length; j++)
           {
               GRR[i][j] = 0;
           }
        }
        
        for(int i = 0; i<IXR.length; i++)
        {
           for(int j = 0; j<IXR[0].length; j++)
           {
               IXR[i][j] = 0;
           }
        }
        
        for(int i = 0; i<PC.length; i++)
        {
            PC[i] = 0;
        }
    }
    
    public void set(int index, String reg_type, String strVal) //index is number of reg
    {
        int dec_val = Integer.parseInt(strVal);
        String bin_val = Integer.toBinaryString(dec_val); // change dec to bin
       
        if(reg_type.toLowerCase().contains("grr") || reg_type.toLowerCase().contains("gen")) //general reg
        {
            bin_val = (bin_val.length()!=16) ? String.format("%0"+ (16-bin_val.length()) + "d",0)+bin_val : bin_val; //add 0
            for(int i = 0; i<GRR[0].length; i++)
            {
               GRR[index][i] = Integer.parseInt(Character.toString(bin_val.charAt(i)));
            }
        }
        
        else if(reg_type.toLowerCase().contains("ixr") || reg_type.toLowerCase().contains("ind")) //index reg
        {
            bin_val = (bin_val.length()!=12) ? String.format("%0"+ (12-bin_val.length()) + "d",0)+bin_val : bin_val; //add 0
            for(int i = 0; i<IXR[0].length; i++)
            {
               IXR[index][i] = Integer.parseInt(Character.toString(bin_val.charAt(i)));
            }
        }
    }
    
    public String get(int index, String reg_type, String val_type)
    {
        if(reg_type.toLowerCase().contains("grr") || reg_type.toLowerCase().contains("gen")) //general reg
        {
        
            String strVal = "";
            for(int i = 0; i<this.GRR[0].length; i++)
            {
               strVal += this.GRR[index][i];
            }
        
            if(val_type.toLowerCase().contains("bin"))
            {
               return strVal; //return binary value
            }
        
            else
            {
               return Integer.toString(Integer.parseInt(strVal,2)); //return dec value
            }
        }
        
        else
        {
            String strVal = "";
            for(int i = 0; i<this.IXR[0].length; i++)
            {
               strVal += this.IXR[index][i];
            }
        
            if(val_type.toLowerCase().contains("bin"))
            {
               return strVal; //return binary value
            }
        
            else
            {
               return Integer.toString(Integer.parseInt(strVal,2)); //return dec value
            }
        }

    }
    
    public int getPCvalue() {        
        return PCvalue;
    }
    
    public void setPCvalue(int value) {
        PCvalue = value;
    }
    
    public void setSentence(int num, String str) {
        PARA[num] = str;
    }
    
    public String getSentence(int num) {
        return PARA[num];
    }
    
    public int getFRValue(int fr) {        
        return FR[fr];
    }
    
    public void setFRValue(int fr, int value) {
        FR[fr] = value;
    }
}
