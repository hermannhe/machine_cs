package Simulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class Panel extends javax.swing.JFrame implements Runnable { //runnable used when IPL pressed
    
    /**
     Main Panel
     */
    public Simulator.Memory m_Memory;
    public Simulator.Register m_Reg;
    public Simulator.Instruction m_Inst;
    public Simulator.Cache m_Cache;
    public String outputbox_content;
    public Thread con_exec_thread;
    public boolean Running;
    
    Map<Integer, JRadioButton> GRR0;
    Map<Integer, JRadioButton> GRR1;
    Map<Integer, JRadioButton> GRR2;
    Map<Integer, JRadioButton> GRR3;
    Map<Integer, JRadioButton> IXR1;
    Map<Integer, JRadioButton> IXR2;
    Map<Integer, JRadioButton> IXR3;
    Map<Integer, JRadioButton> INST;
    FileHandler inst_file;
    
    public void refreshAllButtons() {
        // GRR0
        String strVal = this.m_Reg.get(0, "GRR", "bin");
        for(int i = 0; i<16; i++)
        {
            GRR0.get(i).setSelected(false);
        }
        for(int i = 0; i<16; i++)
        {
            if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
            {
                GRR0.get(i).setSelected(true);
            }
        }
        this.r0val.setText(Integer.toString(Integer.parseInt(strVal, 2)));
        // GRR1
        strVal = this.m_Reg.get(1, "GRR", "bin");
        for(int i = 0; i<16; i++)
        {
            GRR1.get(i).setSelected(false);
        }
        for(int i = 0; i<16; i++)
        {
            if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
            {
                GRR1.get(i).setSelected(true);
            }
        }
        this.r1val.setText(Integer.toString(Integer.parseInt(strVal, 2)));
        // GRR2
        strVal = this.m_Reg.get(2, "GRR", "bin");
        for(int i = 0; i<16; i++)
        {
            GRR2.get(i).setSelected(false);
        }
        for(int i = 0; i<16; i++)
        {
            if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
            {
                GRR2.get(i).setSelected(true);
            }
        }
        this.r2val.setText(Integer.toString(Integer.parseInt(strVal, 2)));
        // GRR3
        strVal = this.m_Reg.get(3, "GRR", "bin");
        for(int i = 0; i<16; i++)
        {
            GRR3.get(i).setSelected(false);
        }
        for(int i = 0; i<16; i++)
        {
            if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
            {
                GRR3.get(i).setSelected(true);
            }
        }
        this.r3val.setText(Integer.toString(Integer.parseInt(strVal, 2)));
        // IXR1
        strVal = this.m_Reg.get(0, "IXR", "bin");
        for(int i = 0; i<12; i++)
        {
            IXR1.get(i).setSelected(false);
        }
        for(int i = 0; i<12; i++)
        {
            if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
            {
                IXR1.get(i).setSelected(true);
            }
        }
        this.i1val.setText(Integer.toString(Integer.parseInt(strVal, 2)));
        // IXR2
        strVal = this.m_Reg.get(1, "IXR", "bin");
        for(int i = 0; i<12; i++)
        {
            IXR2.get(i).setSelected(false);
        }
        for(int i = 0; i<12; i++)
        {
            if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
            {
                IXR2.get(i).setSelected(true);
            }
        }
        this.i2val.setText(Integer.toString(Integer.parseInt(strVal, 2)));
        // IXR3
        strVal = this.m_Reg.get(2, "IXR", "bin");
        for(int i = 0; i<12; i++)
        {
            IXR3.get(i).setSelected(false);
        }
        for(int i = 0; i<12; i++)
        {
            if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
            {
                IXR3.get(i).setSelected(true);
            }
        }
        this.i3val.setText(Integer.toString(Integer.parseInt(strVal, 2)));
    }
    
    public Panel() 
    {
        initComponents();
        this.m_Memory = new Simulator.Memory();
        this.m_Reg = new Simulator.Register();
        this.m_Inst = new Simulator.Instruction(this);
        this.m_Cache = new Simulator.Cache();
        this.inst_file = new FileHandler();
        this.outputbox_content = "";
        this.m_Reg.setPCvalue(4);
        this.Running = false;

        GRR0 = new HashMap<Integer, JRadioButton>();
        GRR0.put(0, this.r00);
        GRR0.put(1, this.r01);
        GRR0.put(2, this.r02);
        GRR0.put(3, this.r03);
        GRR0.put(4, this.r04);
        GRR0.put(5, this.r05);
        GRR0.put(6, this.r06);
        GRR0.put(7, this.r07);
        GRR0.put(8, this.r08);
        GRR0.put(9, this.r09);
        GRR0.put(10, this.r010);
        GRR0.put(11, this.r011);
        GRR0.put(12, this.r012);
        GRR0.put(13, this.r013);
        GRR0.put(14, this.r014);
        GRR0.put(15, this.r015);
        
        GRR1 = new HashMap<Integer, JRadioButton>();
        GRR1.put(0, this.r10);
        GRR1.put(1, this.r11);
        GRR1.put(2, this.r12);
        GRR1.put(3, this.r13);
        GRR1.put(4, this.r14);
        GRR1.put(5, this.r15);
        GRR1.put(6, this.r16);
        GRR1.put(7, this.r17);
        GRR1.put(8, this.r18);
        GRR1.put(9, this.r19);
        GRR1.put(10, this.r110);
        GRR1.put(11, this.r111);
        GRR1.put(12, this.r112);
        GRR1.put(13, this.r113);
        GRR1.put(14, this.r114);
        GRR1.put(15, this.r115);
        
        GRR2 = new HashMap<Integer, JRadioButton>();
        GRR2.put(0, this.r20);
        GRR2.put(1, this.r21);
        GRR2.put(2, this.r22);
        GRR2.put(3, this.r23);
        GRR2.put(4, this.r24);
        GRR2.put(5, this.r25);
        GRR2.put(6, this.r26);
        GRR2.put(7, this.r27);
        GRR2.put(8, this.r28);
        GRR2.put(9, this.r29);
        GRR2.put(10, this.r210);
        GRR2.put(11, this.r211);
        GRR2.put(12, this.r212);
        GRR2.put(13, this.r213);
        GRR2.put(14, this.r214);
        GRR2.put(15, this.r215);
        
        GRR3 = new HashMap<Integer, JRadioButton>();
        GRR3.put(0, this.r30);
        GRR3.put(1, this.r31);
        GRR3.put(2, this.r32);
        GRR3.put(3, this.r33);
        GRR3.put(4, this.r34);
        GRR3.put(5, this.r35);
        GRR3.put(6, this.r36);
        GRR3.put(7, this.r37);
        GRR3.put(8, this.r38);
        GRR3.put(9, this.r39);
        GRR3.put(10, this.r310);
        GRR3.put(11, this.r311);
        GRR3.put(12, this.r312);
        GRR3.put(13, this.r313);
        GRR3.put(14, this.r314);
        GRR3.put(15, this.r315);
        
        IXR1 = new HashMap<Integer, JRadioButton>();
        IXR1.put(0, this.i10);
        IXR1.put(1, this.i11);
        IXR1.put(2, this.i12);
        IXR1.put(3, this.i13);
        IXR1.put(4, this.i14);
        IXR1.put(5, this.i15);
        IXR1.put(6, this.i16);
        IXR1.put(7, this.i17);
        IXR1.put(8, this.i18);
        IXR1.put(9, this.i19);
        IXR1.put(10, this.i110);
        IXR1.put(11, this.i111);
        
        IXR2 = new HashMap<Integer, JRadioButton>();
        IXR2.put(0, this.i20);
        IXR2.put(1, this.i21);
        IXR2.put(2, this.i22);
        IXR2.put(3, this.i23);
        IXR2.put(4, this.i24);
        IXR2.put(5, this.i25);
        IXR2.put(6, this.i26);
        IXR2.put(7, this.i27);
        IXR2.put(8, this.i28);
        IXR2.put(9, this.i29);
        IXR2.put(10, this.i210);
        IXR2.put(11, this.i211);
        
        IXR3 = new HashMap<Integer, JRadioButton>();
        IXR3.put(0, this.i30);
        IXR3.put(1, this.i31);
        IXR3.put(2, this.i32);
        IXR3.put(3, this.i33);
        IXR3.put(4, this.i34);
        IXR3.put(5, this.i35);
        IXR3.put(6, this.i36);
        IXR3.put(7, this.i37);
        IXR3.put(8, this.i38);
        IXR3.put(9, this.i39);
        IXR3.put(10, this.i310);
        IXR3.put(11, this.i311);
        
        INST = new HashMap<Integer, JRadioButton>();
        INST.put(0, this.inst1);
        INST.put(1, this.inst2);
        INST.put(2, this.inst3);
        INST.put(3, this.inst4);
        INST.put(4, this.inst5);
        INST.put(5, this.inst6);
        INST.put(6, this.inst7);
        INST.put(7, this.inst8);
        INST.put(8, this.inst9);
        INST.put(9, this.inst10);
        INST.put(10, this.inst11);
        INST.put(11, this.inst12);
        INST.put(12, this.inst13);
        INST.put(13, this.inst14);
        INST.put(14, this.inst15);
        INST.put(15, this.inst16);
        
        this.tf_startaddr.getDocument().addDocumentListener(new DocumentListener() //add event listener to start address text field
        {


            public void insertUpdate(DocumentEvent e) //if the start address text field value has changed
            {
                m_Reg.setPCvalue(Integer.parseInt(tf_startaddr.getText())); //reset the start address
            }

            public void removeUpdate(DocumentEvent e) {

            }

            public void changedUpdate(DocumentEvent e) {
                
            }
        });
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItemPaste = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        sigstp_exc = new javax.swing.JButton();
        inst1 = new javax.swing.JRadioButton();
        inst2 = new javax.swing.JRadioButton();
        inst3 = new javax.swing.JRadioButton();
        inst5 = new javax.swing.JRadioButton();
        inst4 = new javax.swing.JRadioButton();
        inst6 = new javax.swing.JRadioButton();
        inst7 = new javax.swing.JRadioButton();
        inst8 = new javax.swing.JRadioButton();
        inst9 = new javax.swing.JRadioButton();
        inst10 = new javax.swing.JRadioButton();
        inst11 = new javax.swing.JRadioButton();
        inst12 = new javax.swing.JRadioButton();
        inst13 = new javax.swing.JRadioButton();
        inst16 = new javax.swing.JRadioButton();
        inst15 = new javax.swing.JRadioButton();
        inst14 = new javax.swing.JRadioButton();
        jLabel15 = new javax.swing.JLabel();
        Show_Opcode = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        sigstp_exc1 = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        Show_PC = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        tf_startaddr = new javax.swing.JTextField();
        IPL = new javax.swing.JToggleButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        r00 = new javax.swing.JRadioButton();
        r01 = new javax.swing.JRadioButton();
        r02 = new javax.swing.JRadioButton();
        r03 = new javax.swing.JRadioButton();
        r04 = new javax.swing.JRadioButton();
        r05 = new javax.swing.JRadioButton();
        r06 = new javax.swing.JRadioButton();
        r07 = new javax.swing.JRadioButton();
        r09 = new javax.swing.JRadioButton();
        r08 = new javax.swing.JRadioButton();
        r010 = new javax.swing.JRadioButton();
        r011 = new javax.swing.JRadioButton();
        r012 = new javax.swing.JRadioButton();
        r013 = new javax.swing.JRadioButton();
        r014 = new javax.swing.JRadioButton();
        r015 = new javax.swing.JRadioButton();
        r10 = new javax.swing.JRadioButton();
        r11 = new javax.swing.JRadioButton();
        r12 = new javax.swing.JRadioButton();
        r13 = new javax.swing.JRadioButton();
        r14 = new javax.swing.JRadioButton();
        r15 = new javax.swing.JRadioButton();
        r16 = new javax.swing.JRadioButton();
        r17 = new javax.swing.JRadioButton();
        r18 = new javax.swing.JRadioButton();
        r19 = new javax.swing.JRadioButton();
        r110 = new javax.swing.JRadioButton();
        r111 = new javax.swing.JRadioButton();
        r112 = new javax.swing.JRadioButton();
        r113 = new javax.swing.JRadioButton();
        r114 = new javax.swing.JRadioButton();
        r115 = new javax.swing.JRadioButton();
        r20 = new javax.swing.JRadioButton();
        r21 = new javax.swing.JRadioButton();
        r22 = new javax.swing.JRadioButton();
        r23 = new javax.swing.JRadioButton();
        r24 = new javax.swing.JRadioButton();
        r25 = new javax.swing.JRadioButton();
        r26 = new javax.swing.JRadioButton();
        r27 = new javax.swing.JRadioButton();
        r28 = new javax.swing.JRadioButton();
        r29 = new javax.swing.JRadioButton();
        r210 = new javax.swing.JRadioButton();
        r211 = new javax.swing.JRadioButton();
        r212 = new javax.swing.JRadioButton();
        r213 = new javax.swing.JRadioButton();
        r214 = new javax.swing.JRadioButton();
        r215 = new javax.swing.JRadioButton();
        r30 = new javax.swing.JRadioButton();
        r31 = new javax.swing.JRadioButton();
        r32 = new javax.swing.JRadioButton();
        r33 = new javax.swing.JRadioButton();
        r34 = new javax.swing.JRadioButton();
        r35 = new javax.swing.JRadioButton();
        r36 = new javax.swing.JRadioButton();
        r37 = new javax.swing.JRadioButton();
        r38 = new javax.swing.JRadioButton();
        r39 = new javax.swing.JRadioButton();
        r310 = new javax.swing.JRadioButton();
        r311 = new javax.swing.JRadioButton();
        r312 = new javax.swing.JRadioButton();
        r313 = new javax.swing.JRadioButton();
        r314 = new javax.swing.JRadioButton();
        r315 = new javax.swing.JRadioButton();
        i10 = new javax.swing.JRadioButton();
        i11 = new javax.swing.JRadioButton();
        i12 = new javax.swing.JRadioButton();
        i13 = new javax.swing.JRadioButton();
        i14 = new javax.swing.JRadioButton();
        i15 = new javax.swing.JRadioButton();
        i16 = new javax.swing.JRadioButton();
        i17 = new javax.swing.JRadioButton();
        i18 = new javax.swing.JRadioButton();
        i19 = new javax.swing.JRadioButton();
        i110 = new javax.swing.JRadioButton();
        i111 = new javax.swing.JRadioButton();
        i20 = new javax.swing.JRadioButton();
        i21 = new javax.swing.JRadioButton();
        i22 = new javax.swing.JRadioButton();
        i23 = new javax.swing.JRadioButton();
        i24 = new javax.swing.JRadioButton();
        i25 = new javax.swing.JRadioButton();
        i26 = new javax.swing.JRadioButton();
        i27 = new javax.swing.JRadioButton();
        i28 = new javax.swing.JRadioButton();
        i29 = new javax.swing.JRadioButton();
        i210 = new javax.swing.JRadioButton();
        i211 = new javax.swing.JRadioButton();
        i30 = new javax.swing.JRadioButton();
        i31 = new javax.swing.JRadioButton();
        i32 = new javax.swing.JRadioButton();
        i33 = new javax.swing.JRadioButton();
        i34 = new javax.swing.JRadioButton();
        i35 = new javax.swing.JRadioButton();
        i36 = new javax.swing.JRadioButton();
        i37 = new javax.swing.JRadioButton();
        i38 = new javax.swing.JRadioButton();
        i39 = new javax.swing.JRadioButton();
        i310 = new javax.swing.JRadioButton();
        i311 = new javax.swing.JRadioButton();
        r0val = new javax.swing.JTextField();
        r0ent = new javax.swing.JButton();
        r1ent = new javax.swing.JButton();
        r2ent = new javax.swing.JButton();
        r3ebt = new javax.swing.JButton();
        i1ent = new javax.swing.JButton();
        i2ent = new javax.swing.JButton();
        r1val = new javax.swing.JTextField();
        r2val = new javax.swing.JTextField();
        r3val = new javax.swing.JTextField();
        i1val = new javax.swing.JTextField();
        i2val = new javax.swing.JTextField();
        i3val = new javax.swing.JTextField();
        i3ent = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        SvAddr = new javax.swing.JTextField();
        SvData = new javax.swing.JTextField();
        SvMemData = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        RdMemData = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        LdAddr = new javax.swing.JTextField();
        LdVal = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        show_filepath = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        SaveFileToMemory = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        OutputBox = new javax.swing.JTextArea();
        jLabel21 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        InputBox = new javax.swing.JTextArea();
        jLabel20 = new javax.swing.JLabel();

        jMenuItemPaste.setText("Paste");
        jMenuItemPaste.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPasteActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItemPaste);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CPU Panel");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        sigstp_exc.setText("Exec Instruction ");
        sigstp_exc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Exec_Inst(evt);
            }
        });

        inst1.setText("0");
        inst1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inst1ActionPerformed(evt);
            }
        });

        inst2.setText("1");

        inst3.setText("2");
        inst3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inst3ActionPerformed(evt);
            }
        });

        inst5.setText("4");

        inst4.setText("3");
        inst4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inst4ActionPerformed(evt);
            }
        });

        inst6.setText("5");
        inst6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inst6ActionPerformed(evt);
            }
        });

        inst7.setText("6");

        inst8.setText("7");

        inst9.setText("8");
        inst9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inst9ActionPerformed(evt);
            }
        });

        inst10.setText("9");

        inst11.setText("10");
        inst11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inst11ActionPerformed(evt);
            }
        });

        inst12.setText("11");

        inst13.setText("12");

        inst16.setText("15");

        inst15.setText("14");

        inst14.setText("13");

        jLabel15.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("OPCODE");

        Show_Opcode.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        Show_Opcode.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Show_Opcode.setText("000000");

        jLabel17.setFont(new java.awt.Font("Lucida Bright", 0, 18)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("Instruction");

        sigstp_exc1.setText("Sigle Step Exec");
        sigstp_exc1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sigstp_exc1ActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("Program Counter");

        Show_PC.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        Show_PC.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Show_PC.setText("000000000100");

        jLabel19.setText("Starting Address");

        tf_startaddr.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        tf_startaddr.setText("0");
        tf_startaddr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_startaddrActionPerformed(evt);
            }
        });

        IPL.setText("IPL");
        IPL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IPLActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(23, 23, 23)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(IPL, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 163, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(sigstp_exc, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 163, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(inst1)
                                    .add(inst5)
                                    .add(inst9)
                                    .add(inst12)
                                    .add(inst15))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(inst2)
                                    .add(inst6)
                                    .add(inst10)
                                    .add(inst13)
                                    .add(inst16))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(inst14)
                                    .add(inst11)
                                    .add(jPanel1Layout.createSequentialGroup()
                                        .add(inst3)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(inst4))
                                    .add(jPanel1Layout.createSequentialGroup()
                                        .add(inst7)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(inst8))))
                            .add(sigstp_exc1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 163, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                                .add(jLabel17)
                                .add(41, 41, 41)))
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(42, 42, 42)
                                .add(tf_startaddr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 67, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel16)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(Show_Opcode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 108, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(jLabel15))
                                    .add(20, 20, 20))
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, Show_PC, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 142, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(16, 16, 16)
                                .add(jLabel19)))))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(new java.awt.Component[] {Show_Opcode, jLabel15}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jLabel17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 42, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(inst1)
                            .add(inst2)
                            .add(inst3)
                            .add(inst4))
                        .add(6, 6, 6)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(inst5)
                            .add(inst6)
                            .add(inst7)
                            .add(inst8))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(inst9)
                            .add(inst10)
                            .add(inst11))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(inst12)
                            .add(inst13)
                            .add(inst14))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(inst15)
                            .add(inst16)))
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jLabel15)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(Show_Opcode)
                        .add(27, 27, 27)
                        .add(jLabel16)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(Show_PC)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jLabel19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(tf_startaddr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(sigstp_exc)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(sigstp_exc1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(IPL))))
        );

        jPanel1Layout.linkSize(new java.awt.Component[] {Show_Opcode, jLabel15}, org.jdesktop.layout.GroupLayout.VERTICAL);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel1.setFont(new java.awt.Font("Lucida Bright", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Registers");

        jLabel2.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel2.setText("R0");

        jLabel3.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel3.setText("R1");

        jLabel4.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel4.setText("R2");

        jLabel5.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel5.setText("R3");

        jLabel6.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel6.setText("IX3");

        jLabel7.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel7.setText("IX2");

        jLabel8.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel8.setText("IX1");

        r00.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r00ActionPerformed(evt);
            }
        });

        r07.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r07ActionPerformed(evt);
            }
        });

        r17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r17ActionPerformed(evt);
            }
        });

        r27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r27ActionPerformed(evt);
            }
        });

        r37.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r37ActionPerformed(evt);
            }
        });

        i17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i17ActionPerformed(evt);
            }
        });

        i27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i27ActionPerformed(evt);
            }
        });

        i37.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i37ActionPerformed(evt);
            }
        });

        r0val.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r0valActionPerformed(evt);
            }
        });

        r0ent.setText("Enter");
        r0ent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r0entActionPerformed(evt);
            }
        });

        r1ent.setText("Enter");
        r1ent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r1entActionPerformed(evt);
            }
        });

        r2ent.setText("Enter");
        r2ent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r2entActionPerformed(evt);
            }
        });

        r3ebt.setText("Enter");
        r3ebt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r3ebtActionPerformed(evt);
            }
        });

        i1ent.setText("Enter");
        i1ent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i1entActionPerformed(evt);
            }
        });

        i2ent.setText("Enter");
        i2ent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i2entActionPerformed(evt);
            }
        });

        r1val.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r1valActionPerformed(evt);
            }
        });

        r2val.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r2valActionPerformed(evt);
            }
        });

        r3val.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r3valActionPerformed(evt);
            }
        });

        i1val.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i1valActionPerformed(evt);
            }
        });

        i2val.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i2valActionPerformed(evt);
            }
        });

        i3val.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i3valActionPerformed(evt);
            }
        });

        i3ent.setText("Enter");
        i3ent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                i3entActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel3)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r10)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r11)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r12)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r13)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r14)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r15)
                        .add(4, 4, 4)
                        .add(r16)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r17)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r18)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r19)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r110)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r111)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r112)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r113)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r114)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r115))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r00)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r01)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r02)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r03)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r04)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r05)
                        .add(4, 4, 4)
                        .add(r06)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r07)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r08)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r09)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r010)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r011)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r012)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r013)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r014)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r015))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel4)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r20)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r21)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r22)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r23)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r24)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r25)
                        .add(4, 4, 4)
                        .add(r26)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r27)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r28)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r29)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r210)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r211)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r212)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r213)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r214)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r215))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel8)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i10)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i11)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i12)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i13)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i14)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i15)
                        .add(4, 4, 4)
                        .add(i16)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i17)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i18)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i19)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i110)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i111))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel7)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i20)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i21)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i22)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i23)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i24)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i25)
                        .add(4, 4, 4)
                        .add(i26)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i27)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i28)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i29)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i210)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i211))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel6)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i30)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i31)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i32)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i33)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i34)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i35)
                        .add(4, 4, 4)
                        .add(i36)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i37)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i38)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i39)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i310)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(i311))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel5)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r30)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r31)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r32)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r33)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r34)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r35)
                        .add(4, 4, 4)
                        .add(r36)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r37)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r38)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r39)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r310)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r311)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r312)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r313)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r314)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(r315)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(i1val)
                    .add(r3val)
                    .add(r2val)
                    .add(r1val)
                    .add(r0val)
                    .add(i2val)
                    .add(i3val))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, r1ent)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, r0ent)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, r2ent)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, i1ent)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, i2ent)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, i3ent)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, r3ebt))
                .addContainerGap())
            .add(jPanel2Layout.createSequentialGroup()
                .add(242, 242, 242)
                .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 119, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(405, 405, 405))
        );

        jPanel2Layout.linkSize(new java.awt.Component[] {i10, i11, i110, i111, i12, i13, i14, i15, i16, i17, i18, i19, i20, i21, i210, i211, i22, i23, i24, i25, i26, i27, i28, i29, i30, i31, i310, i311, i32, i33, i34, i35, i36, i37, i38, i39, jLabel2, jLabel3, jLabel4, jLabel5, jLabel6, jLabel7, jLabel8, r00, r01, r010, r011, r012, r013, r014, r015, r02, r03, r04, r05, r06, r07, r08, r09, r10, r11, r110, r111, r112, r113, r114, r115, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r210, r211, r212, r213, r214, r215, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r310, r311, r312, r313, r314, r315, r32, r33, r34, r35, r36, r37, r38, r39}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 44, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 29, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(r00)
                                .add(r01)
                                .add(r02)
                                .add(r03)
                                .add(r04)
                                .add(r05)
                                .add(r06)
                                .add(r07)
                                .add(r08)
                                .add(r09)
                                .add(r010)
                                .add(r011)
                                .add(r012)
                                .add(r013)
                                .add(r014)
                                .add(r015))
                            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(r0val, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(r0ent)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2Layout.createSequentialGroup()
                                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jLabel3)
                                    .add(r10)
                                    .add(r11)
                                    .add(r12)
                                    .add(r13)
                                    .add(r14)
                                    .add(r15)
                                    .add(r16)
                                    .add(r17)
                                    .add(r18)
                                    .add(r19)
                                    .add(r110)
                                    .add(r111)
                                    .add(r112)
                                    .add(r113)
                                    .add(r114)
                                    .add(r115)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                        .add(r1ent)
                                        .add(r1val, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jLabel4)
                                    .add(r20)
                                    .add(r21)
                                    .add(r22)
                                    .add(r23)
                                    .add(r24)
                                    .add(r25)
                                    .add(r26)
                                    .add(r27)
                                    .add(r28)
                                    .add(r29)
                                    .add(r210)
                                    .add(r211)
                                    .add(r212)
                                    .add(r213)
                                    .add(r214)
                                    .add(r215)
                                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                        .add(r2ent)
                                        .add(r2val, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jLabel5)
                                    .add(r30)
                                    .add(r31)
                                    .add(r32)
                                    .add(r33)
                                    .add(r34)
                                    .add(r35)
                                    .add(r36)
                                    .add(r37)
                                    .add(r38)
                                    .add(r39)
                                    .add(r310)
                                    .add(r311)
                                    .add(r312)
                                    .add(r313)
                                    .add(r314)
                                    .add(r315)
                                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                        .add(r3ebt)
                                        .add(r3val, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jPanel2Layout.createSequentialGroup()
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jLabel8))
                                    .add(jPanel2Layout.createSequentialGroup()
                                        .add(5, 5, 5)
                                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                            .add(i1ent)
                                            .add(i1val, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(i10)
                                .add(i11)
                                .add(i12)
                                .add(i13)
                                .add(i14)
                                .add(i15)
                                .add(i16)
                                .add(i17)
                                .add(i18)
                                .add(i19)
                                .add(i110)
                                .add(i111)))
                        .add(6, 6, 6)
                        .add(i2val, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(2, 2, 2))
                    .add(i2ent)
                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i211)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i210)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i29)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i28)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i27)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i26)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i25)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i24)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i23)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i22)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i21)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, i20)
                        .add(jLabel7)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel6)
                    .add(i30)
                    .add(i31)
                    .add(i32)
                    .add(i33)
                    .add(i34)
                    .add(i35)
                    .add(i36)
                    .add(i37)
                    .add(i38)
                    .add(i39)
                    .add(i310)
                    .add(i311)
                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(i3ent)
                        .add(i3val, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(new java.awt.Component[] {i10, i11, i110, i111, i12, i13, i14, i15, i16, i17, i18, i19, i20, i21, i210, i211, i22, i23, i24, i25, i26, i27, i28, i29, i30, i31, i310, i311, i32, i33, i34, i35, i36, i37, i38, i39, jLabel2, jLabel3, jLabel4, jLabel5, jLabel6, jLabel7, jLabel8, r00, r01, r010, r011, r012, r013, r014, r015, r02, r03, r04, r05, r06, r07, r08, r09, r0ent, r0val, r10, r11, r110, r111, r112, r113, r114, r115, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r210, r211, r212, r213, r214, r215, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r310, r311, r312, r313, r314, r315, r32, r33, r34, r35, r36, r37, r38, r39}, org.jdesktop.layout.GroupLayout.VERTICAL);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jLabel9.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Store Data");

        jLabel10.setText("Address");

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("Value");

        SvAddr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SvAddrActionPerformed(evt);
            }
        });

        SvMemData.setText("Save");
        SvMemData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SvMemDataActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel3Layout.createSequentialGroup()
                        .add(jLabel9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 114, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(171, 171, 171))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel3Layout.createSequentialGroup()
                        .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jLabel10, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jLabel11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(31, 31, 31)
                        .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(SvAddr)
                            .add(SvData, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 126, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(117, 117, 117))))
            .add(jPanel3Layout.createSequentialGroup()
                .add(72, 72, 72)
                .add(SvMemData)
                .add(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(jLabel9)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel10)
                    .add(SvAddr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel11)
                    .add(SvData, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 12, Short.MAX_VALUE)
                .add(SvMemData))
        );

        jPanel3Layout.linkSize(new java.awt.Component[] {SvData, jLabel11}, org.jdesktop.layout.GroupLayout.VERTICAL);

        jPanel3Layout.linkSize(new java.awt.Component[] {SvAddr, jLabel10}, org.jdesktop.layout.GroupLayout.VERTICAL);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        RdMemData.setText("Read");
        RdMemData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RdMemDataActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Load Data");

        jLabel13.setText("Address");

        LdAddr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LdAddrActionPerformed(evt);
            }
        });

        LdVal.setEditable(false);

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Value");

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel4Layout.createSequentialGroup()
                        .add(jLabel12, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 114, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(199, 199, 199))
                    .add(jPanel4Layout.createSequentialGroup()
                        .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jLabel13, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jLabel14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel4Layout.createSequentialGroup()
                                .add(31, 31, 31)
                                .add(LdAddr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 126, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanel4Layout.createSequentialGroup()
                                .add(19, 19, 19)
                                .add(LdVal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 151, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(132, 132, 132))))
            .add(jPanel4Layout.createSequentialGroup()
                .add(75, 75, 75)
                .add(RdMemData)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(jLabel12)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel13)
                    .add(LdAddr, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel14)
                    .add(LdVal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 12, Short.MAX_VALUE)
                .add(RdMemData))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jButton1.setText("Browse\n");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Browser(evt);
            }
        });

        show_filepath.setEditable(false);

        jLabel18.setFont(new java.awt.Font("Lucida Bright", 0, 18)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("Read Instructions From File");

        SaveFileToMemory.setText("Save Into Memory");
        SaveFileToMemory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveFileToMemory(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jLabel18)
                .add(178, 178, 178))
            .add(jPanel5Layout.createSequentialGroup()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(47, 47, 47)
                        .add(jButton1)
                        .add(18, 18, 18)
                        .add(show_filepath, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 418, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(209, 209, 209)
                        .add(SaveFileToMemory)))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .add(9, 9, 9)
                .add(jLabel18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 42, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(show_filepath, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 36, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 37, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(11, 11, 11)
                .add(SaveFileToMemory, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5Layout.linkSize(new java.awt.Component[] {jButton1, show_filepath}, org.jdesktop.layout.GroupLayout.VERTICAL);

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        OutputBox.setEditable(false);
        OutputBox.setColumns(20);
        OutputBox.setRows(5);
        jScrollPane1.setViewportView(OutputBox);

        jLabel21.setText("output");

        InputBox.setColumns(20);
        InputBox.setRows(5);
        InputBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                InputBoxMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                InputBoxMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(InputBox);

        jLabel20.setText("Input");

        org.jdesktop.layout.GroupLayout jPanel7Layout = new org.jdesktop.layout.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .add(281, 281, 281)
                .add(jLabel20)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jLabel21)
                .add(312, 312, 312))
            .add(jPanel7Layout.createSequentialGroup()
                .add(109, 109, 109)
                .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 409, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 326, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(130, 130, 130))
        );

        jPanel7Layout.linkSize(new java.awt.Component[] {jScrollPane1, jScrollPane2}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel7Layout.createSequentialGroup()
                        .add(jLabel20)
                        .add(0, 0, Short.MAX_VALUE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel7Layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(jLabel21)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                    .add(jScrollPane1))
                .addContainerGap())
        );

        jPanel7Layout.linkSize(new java.awt.Component[] {jScrollPane1, jScrollPane2}, org.jdesktop.layout.GroupLayout.VERTICAL);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 262, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 262, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(0, 0, Short.MAX_VALUE))
                    .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        layout.linkSize(new java.awt.Component[] {jPanel3, jPanel4}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        layout.linkSize(new java.awt.Component[] {jPanel3, jPanel4}, org.jdesktop.layout.GroupLayout.VERTICAL);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Exec_Inst(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Exec_Inst
        // TODO add your handling code here:
        String opcode = "";
        String instruction = "";
        for(int i = 0; i <= 5; i++)
        {
            if(this.INST.get(i).isSelected())
            {
                opcode += "1";
            }
            else
            {
                opcode += "0";
            }
        }
        this.Show_Opcode.setText(opcode);
        
        instruction = opcode;
        for(int i = 6; i <= 15; i++)
        {
            if(this.INST.get(i).isSelected())
            {
                instruction += "1";
            }
            else
            {
                instruction += "0";
            }
        }
        
        this.m_Reg.setPCvalue(this.m_Reg.getPCvalue() - 1);
        if (this.m_Inst.ParseInst(instruction) == 0) this.m_Reg.setPCvalue(4);
        else this.m_Reg.setPCvalue(this.m_Reg.getPCvalue() + 1);
        this.refreshAllButtons();
       
        String bin_next_exec_addr = Integer.toBinaryString(this.m_Reg.getPCvalue());//convert to binary
       
        bin_next_exec_addr = (bin_next_exec_addr.length()!=12) ? String.format("%0"+ (12-bin_next_exec_addr.length()) + "d",0)
               +bin_next_exec_addr : bin_next_exec_addr; //add 0
       
        this.Show_PC.setText(bin_next_exec_addr);
        
        for(int i = 0; i <= 15; i++)
        {
            this.INST.get(i).setSelected(false);
        }
    }//GEN-LAST:event_Exec_Inst

    private void inst1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inst1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inst1ActionPerformed

    private void inst3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inst3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inst3ActionPerformed

    private void inst6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inst6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inst6ActionPerformed

    private void inst9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inst9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inst9ActionPerformed

    private void r07ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r07ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r07ActionPerformed

    private void r17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r17ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r17ActionPerformed

    private void r27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r27ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r27ActionPerformed

    private void r37ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r37ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r37ActionPerformed

    private void i17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i17ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_i17ActionPerformed

    private void i27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i27ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_i27ActionPerformed

    private void i37ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i37ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_i37ActionPerformed

    private void r0valActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r0valActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r0valActionPerformed

    private void r1valActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r1valActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r1valActionPerformed

    private void r2valActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r2valActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r2valActionPerformed

    private void r3valActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r3valActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r3valActionPerformed

    private void i1valActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i1valActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_i1valActionPerformed

    private void i2valActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i2valActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_i2valActionPerformed

    private void i3valActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i3valActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_i3valActionPerformed

    private void SvAddrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SvAddrActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SvAddrActionPerformed

    private void SvMemDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SvMemDataActionPerformed
        // TODO add your handling code here:
        String strAddr = this.SvAddr.getText();
        String strVal = this.SvData.getText();
        if(!strAddr.matches("") && !strVal.matches("")) //textfields not empty
        {
            this.m_Memory.set(strAddr, strVal, "dec");
            this.SvAddr.setText("");
            this.SvData.setText("");
        }
        refreshAllButtons();
    }//GEN-LAST:event_SvMemDataActionPerformed

    private void RdMemDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RdMemDataActionPerformed
        // TODO add your handling code here:
        String strAddr = this.LdAddr.getText();
        String strVal;
        if(!strAddr.matches(""))
        {
            strVal = this.m_Memory.get(strAddr, "bin");
            this.LdVal.setText(strVal);
        }
        refreshAllButtons();
    }//GEN-LAST:event_RdMemDataActionPerformed

    private void LdAddrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LdAddrActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_LdAddrActionPerformed

    private void inst11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inst11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inst11ActionPerformed

    private void r0entActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r0entActionPerformed
        // TODO add your handling code here:
        if(!this.r0val.getText().matches(""))
        {
            this.m_Reg.set(0, "GRR", this.r0val.getText());
            String strVal = this.m_Reg.get(0, "GRR", "bin");
            for(int i = 0; i<16; i++)
            {
                GRR0.get(i).setSelected(false);
            }
            for(int i = 0; i<16; i++)
            {
                if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
                {
                    GRR0.get(i).setSelected(true);
                }
            }
        }
      
    }//GEN-LAST:event_r0entActionPerformed

    private void r1entActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r1entActionPerformed
        // TODO add your handling code here:
        if(!this.r1val.getText().matches(""))
        {
            this.m_Reg.set(1, "GRR", this.r1val.getText());
            String strVal = this.m_Reg.get(1, "GRR", "bin");
            for(int i = 0; i<16; i++)
            {
                GRR1.get(i).setSelected(false);
            }
            for(int i = 0; i<16; i++)
            {
                if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
                {
                    GRR1.get(i).setSelected(true);
                }
            }
        }
    }//GEN-LAST:event_r1entActionPerformed

    private void r2entActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r2entActionPerformed
        // TODO add your handling code here:
        if(!this.r2val.getText().matches(""))
        {
            this.m_Reg.set(2, "GRR", this.r2val.getText());
            String strVal = this.m_Reg.get(2, "GRR", "bin");
            for(int i = 0; i<16; i++)
            {
                GRR2.get(i).setSelected(false);
            }
            for(int i = 0; i<16; i++)
            {
                if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
                {
                    GRR2.get(i).setSelected(true);
                }
            }
        }
    }//GEN-LAST:event_r2entActionPerformed

    private void r3ebtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r3ebtActionPerformed
        // TODO add your handling code here:
        if(!this.r3val.getText().matches(""))
        {
            this.m_Reg.set(3, "GRR", this.r3val.getText());
            String strVal = this.m_Reg.get(3, "GRR", "bin");
            for(int i = 0; i<16; i++)
            {
                GRR3.get(i).setSelected(false);
            }
            for(int i = 0; i<16; i++)
            {
                if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
                {
                    GRR3.get(i).setSelected(true);
                }
            }
        }
    }//GEN-LAST:event_r3ebtActionPerformed

    private void i1entActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i1entActionPerformed
        // TODO add your handling code here:
        if(!this.i1val.getText().matches(""))
        {
            this.m_Reg.set(0, "IXR", this.i1val.getText());
            String strVal = this.m_Reg.get(0, "IXR", "bin");
            for(int i = 0; i<12; i++)
            {
                IXR1.get(i).setSelected(false);
            }
            for(int i = 0; i<12; i++)
            {
                if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
                {
                    IXR1.get(i).setSelected(true);
                }
            }
        }
    }//GEN-LAST:event_i1entActionPerformed

    private void i2entActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i2entActionPerformed
        // TODO add your handling code here:
        if(!this.i2val.getText().matches(""))
        {
            this.m_Reg.set(1, "IXR", this.i2val.getText());
            String strVal = this.m_Reg.get(1, "IXR", "bin");
            for(int i = 0; i<12; i++)
            {
                IXR2.get(i).setSelected(false);
            }
            for(int i = 0; i<12; i++)
            {
                if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
                {
                    IXR2.get(i).setSelected(true);
                }
            }
        }
    }//GEN-LAST:event_i2entActionPerformed

    private void i3entActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_i3entActionPerformed
        // TODO add your handling code here:
        if(!this.i3val.getText().matches(""))
        {
            this.m_Reg.set(2, "IXR", this.i3val.getText());
            String strVal = this.m_Reg.get(2, "IXR", "bin");
            for(int i = 0; i<12; i++)
            {
                IXR3.get(i).setSelected(false);
            }
            for(int i = 0; i<12; i++)
            {
                if(Integer.parseInt(Character.toString(strVal.charAt(i))) == 1)
                {
                    IXR3.get(i).setSelected(true);
                }
            }
        }
    }//GEN-LAST:event_i3entActionPerformed

    private void r00ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r00ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r00ActionPerformed

    private void sigstp_exc1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sigstp_exc1ActionPerformed
        // TODO add your handling code here:
        String bin_current_exec_addr = Integer.toBinaryString(this.m_Reg.getPCvalue());//convert to binary
       
        bin_current_exec_addr = (bin_current_exec_addr.length()!=12) ? String.format("%0"+ (12-bin_current_exec_addr.length()) + "d",0)
               +bin_current_exec_addr : bin_current_exec_addr; //add 0
              
        String instruction = this.m_Memory.get(Integer.toString(this.m_Reg.getPCvalue()), "bin");
        if (this.m_Inst.ParseInst(instruction) == 0) this.m_Reg.setPCvalue(4);
        else this.m_Reg.setPCvalue(this.m_Reg.getPCvalue() + 1);
        this.refreshAllButtons();
       
        String bin_next_exec_addr = Integer.toBinaryString(this.m_Reg.getPCvalue());//convert to binary
       
        bin_next_exec_addr = (bin_next_exec_addr.length()!=12) ? String.format("%0"+ (12-bin_next_exec_addr.length()) + "d",0)
               +bin_next_exec_addr : bin_next_exec_addr; //add 0
       
        this.Show_PC.setText(bin_next_exec_addr);
    }//GEN-LAST:event_sigstp_exc1ActionPerformed

    private void Browser(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Browser
        // TODO add your handling code here:
        this.show_filepath.setText(this.inst_file.LoadFile());
    }//GEN-LAST:event_Browser

    private void SaveFileToMemory(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveFileToMemory
        ArrayList<String> file_content;
        try 
        {
            file_content = this.inst_file.ParseFile();
        }
        catch (Exception ex)
        {
            return;
        }
        
        try
        {
           Simulator.FileToMemory filetomem_panel = new Simulator.FileToMemory(this.m_Memory, file_content);
           filetomem_panel.setVisible(true);
           file_content.clear();
        }
        catch(NullPointerException e)
        {
            return;
        }
    }//GEN-LAST:event_SaveFileToMemory

    private void inst4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inst4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inst4ActionPerformed

    private void IPLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IPLActionPerformed
        if(this.IPL.isSelected()) //IPL is pressed down
        {
            this.start();
        }
        else //IPL is pressed up
        {
            this.terminate();
        }
    }//GEN-LAST:event_IPLActionPerformed

    private void tf_startaddrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tf_startaddrActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tf_startaddrActionPerformed

    private void InputBoxMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InputBoxMousePressed
        // TODO add your handling code here:
        if (evt.isPopupTrigger()) {
            this.jPopupMenu1.show(this.InputBox, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_InputBoxMousePressed

    private void InputBoxMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InputBoxMouseReleased
        // TODO add your handling code here:
        if (evt.isPopupTrigger()) {
            this.jPopupMenu1.show(this.InputBox, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_InputBoxMouseReleased

    private void jMenuItemPasteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPasteActionPerformed
        // TODO add your handling code here:
        this.InputBox.paste();
    }//GEN-LAST:event_jMenuItemPasteActionPerformed

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            javax.swing.UIManager.LookAndFeelInfo[] installedLookAndFeels=javax.swing.UIManager.getInstalledLookAndFeels();
            for (int idx=0; idx<installedLookAndFeels.length; idx++)
                if ("Nimbus".equals(installedLookAndFeels[idx].getName())) {
                    javax.swing.UIManager.setLookAndFeel(installedLookAndFeels[idx].getClassName());
                    break;
                }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Panel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Panel().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton IPL;
    public javax.swing.JTextArea InputBox;
    private javax.swing.JTextField LdAddr;
    private javax.swing.JTextField LdVal;
    public javax.swing.JTextArea OutputBox;
    private javax.swing.JButton RdMemData;
    private javax.swing.JButton SaveFileToMemory;
    private javax.swing.JLabel Show_Opcode;
    private javax.swing.JLabel Show_PC;
    private javax.swing.JTextField SvAddr;
    private javax.swing.JTextField SvData;
    private javax.swing.JButton SvMemData;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton i10;
    private javax.swing.JRadioButton i11;
    private javax.swing.JRadioButton i110;
    private javax.swing.JRadioButton i111;
    private javax.swing.JRadioButton i12;
    private javax.swing.JRadioButton i13;
    private javax.swing.JRadioButton i14;
    private javax.swing.JRadioButton i15;
    private javax.swing.JRadioButton i16;
    private javax.swing.JRadioButton i17;
    private javax.swing.JRadioButton i18;
    private javax.swing.JRadioButton i19;
    private javax.swing.JButton i1ent;
    private javax.swing.JTextField i1val;
    private javax.swing.JRadioButton i20;
    private javax.swing.JRadioButton i21;
    private javax.swing.JRadioButton i210;
    private javax.swing.JRadioButton i211;
    private javax.swing.JRadioButton i22;
    private javax.swing.JRadioButton i23;
    private javax.swing.JRadioButton i24;
    private javax.swing.JRadioButton i25;
    private javax.swing.JRadioButton i26;
    private javax.swing.JRadioButton i27;
    private javax.swing.JRadioButton i28;
    private javax.swing.JRadioButton i29;
    private javax.swing.JButton i2ent;
    private javax.swing.JTextField i2val;
    private javax.swing.JRadioButton i30;
    private javax.swing.JRadioButton i31;
    private javax.swing.JRadioButton i310;
    private javax.swing.JRadioButton i311;
    private javax.swing.JRadioButton i32;
    private javax.swing.JRadioButton i33;
    private javax.swing.JRadioButton i34;
    private javax.swing.JRadioButton i35;
    private javax.swing.JRadioButton i36;
    private javax.swing.JRadioButton i37;
    private javax.swing.JRadioButton i38;
    private javax.swing.JRadioButton i39;
    private javax.swing.JButton i3ent;
    private javax.swing.JTextField i3val;
    private javax.swing.JRadioButton inst1;
    private javax.swing.JRadioButton inst10;
    private javax.swing.JRadioButton inst11;
    private javax.swing.JRadioButton inst12;
    private javax.swing.JRadioButton inst13;
    private javax.swing.JRadioButton inst14;
    private javax.swing.JRadioButton inst15;
    private javax.swing.JRadioButton inst16;
    private javax.swing.JRadioButton inst2;
    private javax.swing.JRadioButton inst3;
    private javax.swing.JRadioButton inst4;
    private javax.swing.JRadioButton inst5;
    private javax.swing.JRadioButton inst6;
    private javax.swing.JRadioButton inst7;
    private javax.swing.JRadioButton inst8;
    private javax.swing.JRadioButton inst9;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItemPaste;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JRadioButton r00;
    private javax.swing.JRadioButton r01;
    private javax.swing.JRadioButton r010;
    private javax.swing.JRadioButton r011;
    private javax.swing.JRadioButton r012;
    private javax.swing.JRadioButton r013;
    private javax.swing.JRadioButton r014;
    private javax.swing.JRadioButton r015;
    private javax.swing.JRadioButton r02;
    private javax.swing.JRadioButton r03;
    private javax.swing.JRadioButton r04;
    private javax.swing.JRadioButton r05;
    private javax.swing.JRadioButton r06;
    private javax.swing.JRadioButton r07;
    private javax.swing.JRadioButton r08;
    private javax.swing.JRadioButton r09;
    private javax.swing.JButton r0ent;
    private javax.swing.JTextField r0val;
    private javax.swing.JRadioButton r10;
    private javax.swing.JRadioButton r11;
    private javax.swing.JRadioButton r110;
    private javax.swing.JRadioButton r111;
    private javax.swing.JRadioButton r112;
    private javax.swing.JRadioButton r113;
    private javax.swing.JRadioButton r114;
    private javax.swing.JRadioButton r115;
    private javax.swing.JRadioButton r12;
    private javax.swing.JRadioButton r13;
    private javax.swing.JRadioButton r14;
    private javax.swing.JRadioButton r15;
    private javax.swing.JRadioButton r16;
    private javax.swing.JRadioButton r17;
    private javax.swing.JRadioButton r18;
    private javax.swing.JRadioButton r19;
    private javax.swing.JButton r1ent;
    private javax.swing.JTextField r1val;
    private javax.swing.JRadioButton r20;
    private javax.swing.JRadioButton r21;
    private javax.swing.JRadioButton r210;
    private javax.swing.JRadioButton r211;
    private javax.swing.JRadioButton r212;
    private javax.swing.JRadioButton r213;
    private javax.swing.JRadioButton r214;
    private javax.swing.JRadioButton r215;
    private javax.swing.JRadioButton r22;
    private javax.swing.JRadioButton r23;
    private javax.swing.JRadioButton r24;
    private javax.swing.JRadioButton r25;
    private javax.swing.JRadioButton r26;
    private javax.swing.JRadioButton r27;
    private javax.swing.JRadioButton r28;
    private javax.swing.JRadioButton r29;
    private javax.swing.JButton r2ent;
    private javax.swing.JTextField r2val;
    private javax.swing.JRadioButton r30;
    private javax.swing.JRadioButton r31;
    private javax.swing.JRadioButton r310;
    private javax.swing.JRadioButton r311;
    private javax.swing.JRadioButton r312;
    private javax.swing.JRadioButton r313;
    private javax.swing.JRadioButton r314;
    private javax.swing.JRadioButton r315;
    private javax.swing.JRadioButton r32;
    private javax.swing.JRadioButton r33;
    private javax.swing.JRadioButton r34;
    private javax.swing.JRadioButton r35;
    private javax.swing.JRadioButton r36;
    private javax.swing.JRadioButton r37;
    private javax.swing.JRadioButton r38;
    private javax.swing.JRadioButton r39;
    private javax.swing.JButton r3ebt;
    private javax.swing.JTextField r3val;
    private javax.swing.JTextField show_filepath;
    private javax.swing.JButton sigstp_exc;
    private javax.swing.JButton sigstp_exc1;
    private javax.swing.JTextField tf_startaddr;
    // End of variables declaration//GEN-END:variables

    public String readfromInputBox() //return one line from the input box
    {
        String all = this.InputBox.getText();
        if(all.matches("") || all == null)
        {
            return "";
        }
        else
        {
            String lines[] = all.split("\\r?\\n");
            all = "";
            for(int i = 1; i<lines.length; i++)
            {
               all += lines[i] + "\n";
            }
            this.InputBox.setText(all); //remove the first line from input box
            return lines[0];//return first line
        }
    }
    
    public void writeOutputBox(String strContent) //add one line to the ouput box
    {
        this.outputbox_content += strContent + "\n";
        this.OutputBox.setText(this.outputbox_content);
    }
    
    public void start()
    {
        this.con_exec_thread = new Thread(this);
        this.Running = true;
        this.con_exec_thread.start();
    }
    
    public void terminate()
    {
        this.Running = false;
    }
    public void run() 
    {
        while(this.Running)
        {
            String instructions = this.m_Memory.get(Integer.toString(this.m_Reg.getPCvalue()), "bin");
            
            int skip = 0;
            if(instructions.substring(0, 6).matches("000000")) //a halt
            {
                this.m_Reg.setPCvalue(4);
                JOptionPane.showMessageDialog(this, "Program has halted");
                this.IPL.setSelected(false);
                this.Running = false;
                skip = 1;
            }
        
            if (skip == 0) {
                this.m_Inst.ParseInst(instructions);
                this.m_Reg.setPCvalue(this.m_Reg.getPCvalue() + 1);
            }
            this.refreshAllButtons();
       
            String bin_next_exec_addr = Integer.toBinaryString(this.m_Reg.getPCvalue());//convert to binary
       
            bin_next_exec_addr = (bin_next_exec_addr.length()!=12) ? String.format("%0"+ (12-bin_next_exec_addr.length()) + "d",0)
               +bin_next_exec_addr : bin_next_exec_addr; //add 0
       
            this.Show_PC.setText(bin_next_exec_addr);
            if (skip == 1) return;
            
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
