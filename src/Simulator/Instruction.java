
package Simulator;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 Instruction class
 */
public class Instruction
{
    final private Simulator.Memory m_Mem;
    final private Simulator.Register m_Reg;
    final private Simulator.Panel m_Pan;
    
    //**********set of opcodes************
    public static final int HLT = 0;
    public static final int LDR = 1;
    public static final int STR = 2;
    public static final int LDA = 3;
    public static final int AMR = 4;
    public static final int SMR = 5;
    public static final int AIR = 6;
    public static final int SIR = 7;
    public static final int JZ = 10;
    public static final int JNE = 11;
    public static final int JCC = 12;
    public static final int JMA = 13;
    public static final int JSR = 14;
    public static final int RFS = 15;
    public static final int SOB = 16;
    public static final int JGE = 17;
    public static final int MLT = 20;
    public static final int DVD = 21;
    public static final int TRR = 22;
    public static final int AND = 23;
    public static final int ORR = 24;
    public static final int NOT = 25;
    public static final int SRC = 31;
    public static final int RRC = 32;
    public static final int FADD = 33;
    public static final int FSUB = 34;
    public static final int VADD = 35;
    public static final int VSUB = 36;
    public static final int LDX = 41;
    public static final int STX = 42;
    public static final int LDFR = 50;
    public static final int STFR = 51;
    public static final int TRAP = 56;
    public static final int IN = 61;
    public static final int OUT = 62;
    public static final int CHK = 63;
    public static final int TIN = 64;
    public static final int TOUT = 65;
    public static final int TFIND = 66;
    //*********************
    
    Instruction(Simulator.Panel main_panel) //pass memory and register from Panel.java
    {
        this.m_Mem = main_panel.m_Memory;
        this.m_Reg = main_panel.m_Reg;
        this.m_Pan = main_panel;
    }
    
    /* LDR */
    private void Exec_LDR(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        if (numI == 0) {
            /* derect mode */
            int effAdd = Integer.parseInt(Addr, 2);
            if (numX > 0) {
                String strIX = m_Reg.get(numX - 1, "ixr", "bin");
                effAdd += Integer.parseInt(strIX, 2);
            }
            String getMem = m_Mem.get(Integer.toString(effAdd), "bin");
            m_Reg.set(numR, "grr", Integer.toString(Integer.parseInt(getMem, 2)));
        }
        else {
            /* indirect mode */
            int indAdd = Integer.parseInt(Addr, 2);
            if (numX > 0) {
                String strIX = m_Reg.get(numX - 1, "ixr", "bin");
                indAdd += Integer.parseInt(strIX, 2);
            }
            String effAddr = m_Mem.get(Integer.toString(indAdd), "bin");
            String getMem = m_Mem.get(Integer.toString(Integer.parseInt(effAddr, 2)), "bin");
            m_Reg.set(numR, "grr", Integer.toString(Integer.parseInt(getMem, 2)));
        }
    }
    
    /* STR */
    private void Exec_STR(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        if (numI == 0) {
            /* derect mode */
            int effAdd = Integer.parseInt(Addr, 2);
            if (numX > 0) {
                String strIX = m_Reg.get(numX - 1, "ixr", "bin");
                effAdd += Integer.parseInt(strIX, 2);
            }
            String getReg = m_Reg.get(numR, "grr", "bin");
            m_Mem.set(Integer.toString(effAdd), getReg, "bin");
        }
        else {
            /* indirect mode */
            int indAdd = Integer.parseInt(Addr, 2);
            if (numX > 0) {
                String strIX = m_Reg.get(numX - 1, "ixr", "bin");
                indAdd += Integer.parseInt(strIX, 2);
            }
            String effAddr = m_Mem.get(Integer.toString(indAdd), "bin");
            String getReg = m_Reg.get(numR, "grr", "bin");
            m_Mem.set(Integer.toString(Integer.parseInt(effAddr, 2)), getReg, "bin");
        }
    }
    
    /* LDA */
    private void Exec_LDA(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        String getValue;
        // direct or indirect
        if (numI == 0) getValue = Integer.toBinaryString(effAdd);
        else getValue = m_Mem.get(Integer.toString(effAdd), "bin");
        m_Reg.set(numR, "grr", Integer.toString(Integer.parseInt(getValue, 2)));
    }
    
    /* AMR */
    private void Exec_AMR(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        if (numI == 0) {
            /* derect mode */
            int effAdd = Integer.parseInt(Addr, 2);
            if (numX > 0) {
                String strIX = m_Reg.get(numX - 1, "ixr", "bin");
                effAdd += Integer.parseInt(strIX, 2);
            }
            String getMem = m_Mem.get(Integer.toString(effAdd), "bin");
            String getReg = m_Reg.get(numR, "grr", "bin");
            int addThem = Integer.parseInt(getMem, 2) + Integer.parseInt(getReg, 2);
            while (addThem < 0) addThem += 65536;
            addThem %= 65536;
            m_Reg.set(numR, "grr", Integer.toString(addThem));
        }
        else {
            /* indirect mode */
            int indAdd = Integer.parseInt(Addr, 2);
            if (numX > 0) {
                String strIX = m_Reg.get(numX - 1, "ixr", "bin");
                indAdd += Integer.parseInt(strIX, 2);
            }
            String effAddr = m_Mem.get(Integer.toString(indAdd), "bin");
            String getMem = m_Mem.get(Integer.toString(Integer.parseInt(effAddr, 2)), "bin");
            String getReg = m_Reg.get(numR, "grr", "bin");
            int addThem = Integer.parseInt(getMem, 2) + Integer.parseInt(getReg, 2);
            while (addThem < 0) addThem += 65536;
            addThem %= 65536;
            m_Reg.set(numR, "grr", Integer.toString(addThem));
        }
    }
    
    /* SMR */
    private void Exec_SMR(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        if (numI == 0) {
            /* derect mode */
            int effAdd = Integer.parseInt(Addr, 2);
            if (numX > 0) {
                String strIX = m_Reg.get(numX - 1, "ixr", "bin");
                effAdd += Integer.parseInt(strIX, 2);
            }
            String getMem = m_Mem.get(Integer.toString(effAdd), "bin");
            String getReg = m_Reg.get(numR, "grr", "bin");
            int addThem = 0 - Integer.parseInt(getMem, 2) + Integer.parseInt(getReg, 2);
            while (addThem < 0) addThem += 65536;
            addThem %= 65536;
            m_Reg.set(numR, "grr", Integer.toString(addThem));
        }
        else {
            /* indirect mode */
            int indAdd = Integer.parseInt(Addr, 2);
            if (numX > 0) {
                String strIX = m_Reg.get(numX - 1, "ixr", "bin");
                indAdd += Integer.parseInt(strIX, 2);
            }
            String effAddr = m_Mem.get(Integer.toString(indAdd), "bin");
            String getMem = m_Mem.get(Integer.toString(Integer.parseInt(effAddr, 2)), "bin");
            String getReg = m_Reg.get(numR, "grr", "bin");
            int addThem = 0 - Integer.parseInt(getMem, 2) + Integer.parseInt(getReg, 2);
            while (addThem < 0) addThem += 65536;
            addThem %= 65536;
            m_Reg.set(numR, "grr", Integer.toString(addThem));
        }
    }
    
    /* AIR */
    private void Exec_AIR(int numR, String Immed) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        String getReg = m_Reg.get(numR, "grr", "bin");
        int addThem = Integer.parseInt(Immed, 2) + Integer.parseInt(getReg, 2);
        while (addThem < 0) addThem += 65536;
        addThem %= 65536;
        m_Reg.set(numR, "grr", Integer.toString(addThem));
    }
    
    /* SIR */
    private void Exec_SIR(int numR, String Immed) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        String getReg = m_Reg.get(numR, "grr", "bin");
        int addThem = 0 - Integer.parseInt(Immed, 2) + Integer.parseInt(getReg, 2);
        while (addThem < 0) addThem += 65536;
        addThem %= 65536;
        m_Reg.set(numR, "grr", Integer.toString(addThem));
    }
    
    /* JZ */
    private void Exec_JZ(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        if (numI == 1) {
            String getMem = m_Mem.get(Integer.toString(effAdd), "bin");
            effAdd = Integer.parseInt(getMem, 2);
        } 
        String getReg = m_Reg.get(numR, "grr", "bin");
        if (Integer.parseInt(getReg, 2) == 0) m_Reg.setPCvalue(effAdd - 1);
    }
    
    /* JNE */
    private void Exec_JNE(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        if (numI == 1) {
            String getMem = m_Mem.get(Integer.toString(effAdd), "bin");
            effAdd = Integer.parseInt(getMem, 2);
        } 
        String getReg = m_Reg.get(numR, "grr", "bin");
        if (Integer.parseInt(getReg, 2) != 0) m_Reg.setPCvalue(effAdd - 1);
    }
    
    /* JMA */
    private void Exec_JMA(int numX, int numI, String Addr) {
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        if (numI == 1) {
            String getMem = m_Mem.get(Integer.toString(effAdd), "bin");
            effAdd = Integer.parseInt(getMem, 2);
        } 
        m_Reg.setPCvalue(effAdd - 1);
    }
    
    /* JGE */
    private void Exec_JGE(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        if (numI == 1) {
            String getMem = m_Mem.get(Integer.toString(effAdd), "bin");
            effAdd = Integer.parseInt(getMem, 2);
        } 
        String getReg = m_Reg.get(numR, "grr", "bin");
        if (Integer.parseInt(getReg, 2) < 32768) m_Reg.setPCvalue(effAdd - 1);
    }
    
    /* MLT */
    private void Exec_MLT(int numR1, int numR2) {
        if (!((numR1 == 0 && numR2 == 2) || (numR1 == 2 && numR2 == 0))) return;
        String getR1 = m_Reg.get(numR1, "grr", "bin");
        String getR2 = m_Reg.get(numR2, "grr", "bin");
        int result = Integer.parseInt(getR1, 2) * Integer.parseInt(getR2, 2);        
        m_Reg.set(numR1, "grr", Integer.toString(result / 65536 % 65536));
        m_Reg.set(numR1 + 1, "grr", Integer.toString(result % 65536));
    }
    
    /* DVD */
    private void Exec_DVD(int numR1, int numR2) {
        if (!((numR1 == 0 && numR2 == 2) || (numR1 == 2 && numR2 == 0))) return;
        String getR1 = m_Reg.get(numR1, "grr", "bin");
        String getR2 = m_Reg.get(numR2, "grr", "bin");
        if (Integer.parseInt(getR2, 2) == 0) return;
        int result = Integer.parseInt(getR1, 2) / Integer.parseInt(getR2, 2);        
        m_Reg.set(numR1, "grr", Integer.toString(result / 65536 % 65536));
        m_Reg.set(numR1 + 1, "grr", Integer.toString(result % 65536));
    }
    
    /* LDX */
    private void Exec_LDX(int numX, int numI, String Addr) {
        if (numX < 1 || numX >3) return;
        int effAdd = Integer.parseInt(Addr, 2);
        String getValue;
        if (numI == 0) getValue = m_Mem.get(Integer.toString(effAdd), "bin");
        else {
            String getMem = m_Mem.get(Integer.toString(effAdd), "bin");
            getValue = m_Mem.get(Integer.toString(Integer.parseInt(getMem, 2)), "bin");
        }
        m_Reg.set(numX - 1, "ixr", Integer.toString(Integer.parseInt(getValue, 2)));
    }
    
    /* STX */
    private void Exec_STX(int numX, int numI, String Addr) {        
        if (numX < 1 || numX >3) return;
        int effAdd = Integer.parseInt(Addr, 2);
        String getAdd;
        // direct or indirect
        if (numI == 0) getAdd = m_Mem.get(Integer.toString(effAdd), "bin");
        else {
            String getMem = m_Mem.get(Integer.toString(effAdd), "bin");
            getAdd = m_Mem.get(Integer.toString(Integer.parseInt(getMem, 2)), "bin");
        }
        String getReg = m_Reg.get(numX - 1, "ixr", "bin");
        m_Mem.set(Integer.toString(Integer.parseInt(getAdd, 2)), getReg, "bin");
    }
    
    /* IN */
    private void Exec_IN(int numR) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        String inputbox_firstline = this.m_Pan.readfromInputBox();
        if(!inputbox_firstline.matches(""))
        {
            this.m_Reg.set(numR, "grr", inputbox_firstline);
        }
    }
    
    /* OUT */
    private void Exec_OUT(int numR) {
        if (numR < 0 || numR > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        String grr_content = this.m_Reg.get(numR, "grr", "dec");
        this.m_Pan.writeOutputBox(grr_content);
    }
    
    private void show_store(String line) {
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            int a = (int)c;
            String s = Integer.toString(a);
            this.m_Reg.set(i % 4, "grr", s);
            this.m_Pan.refreshAllButtons();
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    /* TIN */
    private void Exec_TIN(String Addr) {
        String inputbox_firstline = this.m_Pan.readfromInputBox();
        show_store(inputbox_firstline);
        this.m_Reg.setSentence(Integer.parseInt(Addr, 2), inputbox_firstline);
    }
    
    /* TOUT */
    private void Exec_TOUT(String Addr) {
        String grr_content = this.m_Reg.getSentence(Integer.parseInt(Addr, 2));
        show_store(grr_content);
        this.m_Pan.writeOutputBox(grr_content);
    }
    
    /* TFIND */
    private void Exec_TFIND(String Addr) {
        String target = this.m_Reg.getSentence(6);
        int tsize = target.length();
        if (tsize < 1) return;
        int num = Integer.parseInt(Addr, 2);
        String sentence = this.m_Reg.getSentence(num);
        int ssize = sentence.length();
        int count = 0;
        show_store(sentence);
        for (int i = 0; i < ssize - tsize + 1; i++) {
            if (sentence.charAt(i) == ' ') count++;
            boolean same = true;
            for (int j = 0; j < tsize; j++) {
                if (sentence.charAt(i + j) != target.charAt(j)) {
                    same = false;
                    break;
                } 
            }
            if (same) {
                this.m_Pan.writeOutputBox(target);
                this.m_Pan.writeOutputBox(Integer.toString(num + 1));
                this.m_Pan.writeOutputBox(Integer.toString(count + 1));
            }
        }
    }

    private void Exec_FADD(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 1) throw new IllegalArgumentException("Invalid Register : " + numR);
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        String getValue = m_Mem.get(Integer.toString(effAdd), "bin");
        if (numI == 1) getValue = m_Mem.get(Integer.toString(Integer.parseInt(getValue, 2)), "bin");
        int newdata = Integer.parseInt(getValue, 2);
        int predata = m_Reg.getFRValue(numR);
        int newmain = newdata % (1 << 8);
        int premain = predata % (1 << 8);
        int newexp = newdata % (1 << 15) / (1 << 8);
        int preexp = newdata % (1 << 15) / (1 << 8);
        int result;
        if (newexp >= preexp) {
            result = (newexp << 8) + (newmain + (premain >> (newexp - preexp)));
        }
        else {
            result = (preexp << 8) + (premain + (newmain >> (preexp - newexp)));
        }
        while (result < 0) result += 65536;
        result %= 65536;
        m_Reg.setFRValue(numR, result);
    }
    
    private void Exec_FSUB(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 1) throw new IllegalArgumentException("Invalid Register : " + numR);
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        String getValue = m_Mem.get(Integer.toString(effAdd), "bin");
        if (numI == 1) getValue = m_Mem.get(Integer.toString(Integer.parseInt(getValue, 2)), "bin");
        int newdata = Integer.parseInt(getValue, 2);
        int predata = m_Reg.getFRValue(numR);
        int newmain = newdata % (1 << 8);
        int premain = predata % (1 << 8);
        int newexp = newdata % (1 << 15) / (1 << 8);
        int preexp = newdata % (1 << 15) / (1 << 8);
        int result;
        if (newexp >= preexp) {
            result = (newexp << 8) + (-newmain + (premain >> (newexp - preexp)));
        }
        else {
            result = (preexp << 8) + (premain + (-newmain >> (preexp - newexp)));
        }
        while (result < 0) result += 65536;
        result %= 65536;
        m_Reg.setFRValue(numR, result);
    }
    
    private void Exec_LDFR(int numR, int numX, int numI, String Addr) {
        if (numR < 0 || numR > 1) throw new IllegalArgumentException("Invalid Register : " + numR);
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        String getValue = m_Mem.get(Integer.toString(effAdd), "bin");
        if (numI == 1) getValue = m_Mem.get(Integer.toString(Integer.parseInt(getValue, 2)), "bin");
        m_Reg.setFRValue(numR, Integer.parseInt(getValue, 2));
    }
    
    private void Exec_STFR(int numR, int numX, int numI, String Addr) {
        if (numX < 0 || numX > 1) throw new IllegalArgumentException("Invalid Register : " + numR);
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        String getAdd = Integer.toString(effAdd, 2);
        if (numI == 1) getAdd = m_Mem.get(Integer.toString(effAdd), "bin");
        int getReg = m_Reg.getFRValue(numR);
        m_Mem.set(Integer.toString(Integer.parseInt(getAdd, 2)), Integer.toString(getReg, 2), "bin");
    }
    
    private void Exec_VADD(int numR, int numX, int numI, String Addr) {
        if (numX < 0 || numX > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        String v1Add = Integer.toString(effAdd, 2);
        String v2Add = Integer.toString(effAdd + 1, 2);
        if (numI == 1) {
            v1Add = m_Mem.get(Integer.toString(effAdd), "bin");
            v2Add = m_Mem.get(Integer.toString(effAdd + 1), "bin");
        }
        String v1value = m_Mem.get(Integer.toString(Integer.parseInt(v1Add, 2)), "bin");
        String v2value = m_Mem.get(Integer.toString(Integer.parseInt(v2Add, 2)), "bin");
        int result = Integer.parseInt(v1value, 2) + Integer.parseInt(v2value, 2);
        while (result < 0) result += 65536;
        result %= 65536;
        m_Mem.set(Integer.toString(Integer.parseInt(v1Add, 2)), Integer.toString(result, 2), "bin");
    }
    
    private void Exec_VSUB(int numR, int numX, int numI, String Addr) {
        if (numX < 0 || numX > 3) throw new IllegalArgumentException("Invalid Register : " + numR);
        int effAdd = Integer.parseInt(Addr, 2);
        if (numX > 0) {
            String strIX = m_Reg.get(numX - 1, "ixr", "bin");
            effAdd += Integer.parseInt(strIX, 2);
        }
        String v1Add = Integer.toString(effAdd, 2);
        String v2Add = Integer.toString(effAdd + 1, 2);
        if (numI == 1) {
            v1Add = m_Mem.get(Integer.toString(effAdd), "bin");
            v2Add = m_Mem.get(Integer.toString(effAdd + 1), "bin");
        }
        String v1value = m_Mem.get(Integer.toString(Integer.parseInt(v1Add, 2)), "bin");
        String v2value = m_Mem.get(Integer.toString(Integer.parseInt(v2Add, 2)), "bin");
        int result = Integer.parseInt(v1value, 2) - Integer.parseInt(v2value, 2);
        while (result < 0) result += 65536;
        result %= 65536;
        m_Mem.set(Integer.toString(Integer.parseInt(v1Add, 2)), Integer.toString(result, 2), "bin");
    }

    /* Parse Instructions into opcode, R, X, I, Address */
    public int ParseInst(String inst) {
        /* Get the number from the Instruction string */
        int opcode = Integer.parseInt(inst.substring(0, 3), 2) * 10 + Integer.parseInt(inst.substring(3, 6), 2);
        int numR = Integer.parseInt(inst.substring(6, 8), 2);
        int numX = Integer.parseInt(inst.substring(8, 10), 2);
        int numI = Integer.parseInt(inst.substring(10, 11), 2);
        String Addr = inst.substring(11, 16);

        /* use switch to excecute operation */
        switch (opcode) {
            case LDR :
                Exec_LDR(numR, numX, numI, Addr);
                break;
            case STR :
                Exec_STR(numR, numX, numI, Addr);
                break;
            case LDA :
                Exec_LDA(numR, numX, numI, Addr);
                break;
            case AMR :
                Exec_AMR(numR, numX, numI, Addr);
                break;
            case SMR :
                Exec_SMR(numR, numX, numI, Addr);
                break;
            case AIR :
                Exec_AIR(numR, Addr);
                break;
            case SIR :
                Exec_SIR(numR, Addr);
                break;
            case JZ :
                Exec_JZ(numR, numX, numI, Addr);
                break;
            case JNE :
                Exec_JNE(numR, numX, numI, Addr);
                break;
            case JMA :
                Exec_JMA(numX, numI, Addr);
                break;
            case JGE :
                Exec_JGE(numR, numX, numI, Addr);
                break;
            case MLT :
                Exec_MLT(numR, numX);
                break;
            case DVD :
                Exec_DVD(numR, numX);
                break;
            case LDX :
                Exec_LDX(numX, numI, Addr);
                break;
            case STX :
                Exec_STX(numX, numI, Addr);
                break;
            case IN :
                Exec_IN(numR);
                break;
            case OUT:
                Exec_OUT(numR);
                break;
            case TIN:
                Exec_TIN(Addr);
                break;
            case TOUT:
                Exec_TOUT(Addr);
                break;
            case TFIND:
                Exec_TFIND(Addr);
                break;
            case FADD :
                Exec_FADD(numR, numX, numI, Addr);
                break;    
            case FSUB :
                Exec_FSUB(numR, numX, numI, Addr);
                break;    
            case LDFR :
                Exec_LDFR(numR, numX, numI, Addr);
                break;    
            case STFR :
                Exec_STFR(numR, numX, numI, Addr);
                break;    
            case VADD :
                Exec_VADD(numR, numX, numI, Addr);
                break;    
            case VSUB :
                Exec_VSUB(numR, numX, numI, Addr);
                break;    
            default:
                return 0;
                
        }
        return 1;
    }
}
