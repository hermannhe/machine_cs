
package Simulator;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class FileHandler 
{
    private JFileChooser file_chooser;
    private ArrayList<String> file_content;
    private String file_path;
    
    FileHandler() //pass instrucion object from Panel.java
    {
        file_content = new ArrayList<String>();
        file_chooser = new JFileChooser(); 
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT FILE","txt"); //only accept txt file
        file_chooser.setFileFilter(filter); // apply filter to file chooser
        file_path = null;
    }
    
   public String LoadFile()
   {
       int returnVal = file_chooser.showOpenDialog(null);
       if(returnVal != JFileChooser.APPROVE_OPTION) //successfully chose the file
       {
          return null;
       }
       else
       {
           this.file_path = file_chooser.getSelectedFile().getAbsolutePath();
           return this.file_path;
       }
   }
   
   public String GetFilePath()
   {
       return file_path;
   }
       
   public ArrayList<String> ParseFile() throws FileNotFoundException
   {
       BufferedReader reader;
       try
       {
          reader = new BufferedReader(new FileReader(this.file_path));
       }
       catch(NullPointerException e)
       {
           JOptionPane.showMessageDialog(null, "No File!");
           return null;
       }
       String line;
        try 
        {
            while ((line = reader.readLine()) != null)
            {
                if(line.length() != 16 || !line.matches("[01]+"))
                {
                    JOptionPane.showMessageDialog(null, "Wrong Format!");
                    return null;
                }
                
                file_content.add(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(FileHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        try
        {
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(FileHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
       return file_content;
   }
}
