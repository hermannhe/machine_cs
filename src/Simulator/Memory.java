
package Simulator;
/*
Memory class
 */
public class Memory 
{
    private int[][] content;
    
    public Memory()
    {
        this.content = new int[4096][16];
        for(int i = 0; i < content.length; i++)
        {
            for(int j = 0; j < content[0].length; j++)
            {
                this.content[i][j] = 0;
            }
        }
    }
    
    public void set(String strAddr, String strVal, String type) //set value to the corresponding memory
    {
        if(Integer.parseInt(strAddr)<0 || Integer.parseInt(strAddr)>4095) //exceeds the address range
        {
            return;
        }
        if(type.toLowerCase().contains("bin"))
        {
            strVal = (strVal.length()!=16) ? String.format("%0"+ (16-strVal.length()) + "d",0)+strVal : strVal;
            for(int i = 0; i<this.content[0].length; i++)
            {
               this.content[Integer.parseInt(strAddr)][i] = Integer.parseInt(Character.toString(strVal.charAt(i)));
            }
        }
        else
        {
            int dec_val = Integer.parseInt(strVal);
            String bin_val = Integer.toBinaryString(dec_val); // change dec to bin
            bin_val = (bin_val.length()!=16) ? String.format("%0"+ (16-bin_val.length()) + "d",0)+bin_val : bin_val; //add 0
            for(int i = 0; i<this.content[0].length; i++)
            {
                this.content[Integer.parseInt(strAddr)][i] = Integer.parseInt(Character.toString(bin_val.charAt(i)));
            }
        }
    }
    
    public String get(String strAddr,String type) //type are bin or dec
    {
        if(Integer.parseInt(strAddr)<0 || Integer.parseInt(strAddr)>4095) //exceeds the address range
        {
            return "";
        }
        String strVal = "";
        for(int i = 0; i<this.content[0].length; i++)
        {
            strVal += this.content[Integer.parseInt(strAddr)][i];
        }
        
        if(type.toLowerCase().contains("bin"))
        {
            return strVal; //return binary value
        }
        
        else
        {
            return Integer.toString(Integer.parseInt(strVal,2)); //return dec value
        }
    }
}
