
package Simulator;

public class Cache 
{
   private int[][] content; //content of the cache
   private final int hit = 1;
   private final int miss = 0;
   private int state;
   
   Cache()
   {
       content = new int[16][32]; //16 lines with 32 bits per line
       state = miss;
   }
   
   public void write_cache(int index, String strAddr, String strVal)
   {
       //parse add
       int addr = Integer.parseInt(strAddr);
       String BinAddr = Integer.toBinaryString(addr); // change dec to bin
       BinAddr = (BinAddr.length()!=16) ? String.format("%0"+ (16-BinAddr.length()) + "d",0)+BinAddr : BinAddr; //add 0
       for(int i = 0; i<16; i++)
       {
           this.content[index][i] = Integer.parseInt(Character.toString(BinAddr.charAt(i)));
       }
       //
       
       //parse value
       int val = Integer.parseInt(strVal);
       String BinVal = Integer.toBinaryString(val); // change dec to bin
       BinVal = (BinVal.length()!=16) ? String.format("%0"+ (16-BinVal.length()) + "d",0)+BinVal : BinVal; //add 0
       for(int i = 16; i<32; i++)
       {
           this.content[index][i] = Integer.parseInt(Character.toString(BinVal.charAt(i)));
       }
       //
   }
   
   public String[] read_cache(String strAddr) //return string array, first is hit/miss, second is the value regarding to strAddr
   {
       //parse addr
       int addr = Integer.parseInt(strAddr);
       String BinAddr = Integer.toBinaryString(addr); // change dec to bin
       BinAddr = (BinAddr.length()!=16) ? String.format("%0"+ (16-BinAddr.length()) + "d",0)+BinAddr : BinAddr; //add 0
       //
       
       //look for if the addr exists
       String strContentAddr = "";
       String strVal = "";
       for(int i = 0; i<this.content.length; i++) 
       {
           for(int j = 0; j<16; j++)
           {
               strContentAddr += Integer.toString(this.content[i][j]);
           }
           
           if(strContentAddr.matches(strAddr))
           {
               this.state = hit; //the address exists
               for(int j = 16; j<32; j++)
               {
                   strVal += Integer.toString(this.content[i][j]);
               } 
               break;
           }
           else
           {
               strContentAddr = "";
           }
       }
       
       String[] result = new String[2];
       if(state == miss)
       {
           
           result[0] = "miss";
           result[1] = strVal;
           return result;
       }
       else
       {
           result[0] = "hit";
           result[1] = strVal;
           return result;
       }
   }
}
